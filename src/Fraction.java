public interface Fraction {
	int getNumerator();
	int getDenominator();
	Fraction add(Fraction b);
	Fraction add(int b);
	Fraction subtract(Fraction b);
	Fraction subtract(int b);
	String toString();
}