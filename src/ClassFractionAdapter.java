
public class ClassFractionAdapter extends LongFraction implements Fraction
{
	public ClassFractionAdapter(long num, long den)
	{
		super(num, den);
	}
	
	private LongFraction getLongFraction(Fraction b)
	{
		return new LongFraction(b.getNumerator(), b.getDenominator());
	}
	
	private Fraction getFraction(LongFraction longFraction)
	{
		return new ClassFractionAdapter(longFraction.numerator(), longFraction.denominator());
	}
	
	@Override
	public int getNumerator()
	{
		return (int)super.numerator();
	}

	@Override
	public int getDenominator()
	{
		return (int)super.denominator();
	}

	@Override
	public Fraction add(Fraction b)
	{
		//LongFraction result = longFraction.plus(((ObjectFractionAdapter)b).longFraction);
		LongFraction longFraction = super.plus(getLongFraction(b));
		return getFraction(longFraction);
	}

	@Override
	public Fraction add(int b)
	{
		LongFraction longFraction = super.plus(b);
		return getFraction(longFraction);
	}

	@Override
	public Fraction subtract(Fraction b)
	{
		LongFraction longFraction = super.minus(getLongFraction(b));
		return getFraction(longFraction);
	}

	@Override
	public Fraction subtract(int b)
	{
		LongFraction longFraction = super.minus(b);
		return getFraction(longFraction);
	}
}