
public class ObjectFractionAdapter implements Fraction
{
	private LongFraction longFraction;
	
	public ObjectFractionAdapter(LongFraction longFraction)
	{
		this.longFraction = longFraction;
	}
	
	public ObjectFractionAdapter(long n, long m)
	{
		this.longFraction = new LongFraction(n, m);
	}
		
	@Override
	public int getNumerator()
	{
		return (int)longFraction.numerator();
	}

	@Override
	public int getDenominator()
	{
		return (int)longFraction.denominator();
	}

	@Override
	public Fraction add(Fraction b)
	{
		LongFraction result = longFraction.plus(((ObjectFractionAdapter)b).longFraction);
		return new ObjectFractionAdapter(result);
	}

	@Override
	public Fraction add(int b)
	{
		return new ObjectFractionAdapter(longFraction.plus(b));
	}

	@Override
	public Fraction subtract(Fraction b)
	{
		LongFraction result = longFraction.minus(((ObjectFractionAdapter)b).longFraction);
		return new ObjectFractionAdapter(result);
	}

	@Override
	public Fraction subtract(int b)
	{
		return new ObjectFractionAdapter(longFraction.minus(b));
	}
	
	@Override
	public String toString()
	{
		return longFraction.toString();
	}
}